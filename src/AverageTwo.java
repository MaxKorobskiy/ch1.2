/**
 * Maxim Korobskiy
 * September 8th, 2016
 * max.korobskiy@gmail.com
 * This program finds the average of 10 integers provided by the user.
 */

import java.util.Scanner; //necessary class imports
import java.util.regex.Pattern;

public class AverageTwo {

	public static void main(String[] args) {
		int i1, i2, i3, i4, i5, i6, i7, i8, i9, i10; //declare 10 variables to add together
		double sum, average; //declare sum and average double variables; decimal values are possible
		String s10; //last integer in String form; to be converted
		Scanner keyboard = new Scanner(System.in); //declare Scanner
		
		keyboard.useDelimiter(Pattern.compile(",")); //allow user input to be delimited by commas
		System.out.println("This program will find the average of 10 integers.\n"
				+ "Enter ten integers delimited by commas:"); //prompting user for integers
		
		i1 = keyboard.nextInt(); //take 10 integers from user input
		i2 = keyboard.nextInt();
		i3 = keyboard.nextInt();
		i4 = keyboard.nextInt();
		i5 = keyboard.nextInt();
		i6 = keyboard.nextInt();
		i7 = keyboard.nextInt();
		i8 = keyboard.nextInt();
		i9 = keyboard.nextInt();
		s10 = keyboard.nextLine();
		i10 = Integer.parseInt(s10.substring(1));
		keyboard.close(); //close input
		
		sum = i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 + i9 + i10; //add integers together
		average = sum / 10; //find average
		System.out.println("The average is " + average); //print average
	}

}