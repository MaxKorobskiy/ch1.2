import java.util.Scanner;

import javax.swing.JOptionPane;

public class Echo{

	/**
	 * Maxim Korobskiy
	 * September 8th, 2016
	 * max.korobskiy@gmail.com
	 * 
	 * This program echoes data types given by user input.
	 */
	public static void main(String[] args) {
		Byte byteValue;
		short shortValue;
		int intValue;
		long longValue;
		float floatValue;
		double doubleValue;
		boolean booleanValue;
		char charValue;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a byte value (-128 to 127):");
		byteValue = keyboard.nextByte();
		
		System.out.println("Enter a short value (-32768 to 32767):");
		shortValue = keyboard.nextShort();
		
		System.out.println("Enter an int value (-2^31 to 2^31-1):");
		intValue = keyboard.nextInt();
		
		System.out.println("Enter a long value (-2^63 to 2^63-1):");
		longValue = keyboard.nextLong();

		System.out.println("Enter a float value (32 bit decimal):");
		floatValue = keyboard.nextFloat();
		
		System.out.println("Enter a double value (64 bit decimal):");
		doubleValue = keyboard.nextDouble();
		
		System.out.println("Enter a boolean value (true or false):");
		booleanValue = keyboard.nextBoolean();
		
		System.out.println("Enter a char value (true or false):");
		charValue = keyboard.next().charAt(0);
		
		JOptionPane.showMessageDialog(null, "The byte you entered is " + byteValue + "\nThe short you entered is " + shortValue + "\nThe int you entered is " + intValue + "\nThe long you entered is " + longValue + "\nThe float you entered is " + floatValue + "\nThe double you entered is " + doubleValue + "\nThe boolean you entered is " + booleanValue + "\nThe char you entered is " + charValue);
		
		keyboard.close();
	}

}
