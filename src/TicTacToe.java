import java.awt.Font;

import java.awt.Graphics;
import javax.swing.JApplet;

public class TicTacToe extends JApplet {

	public void paint(Graphics canvas) {
		
		Font font = new Font("Arial", Font.PLAIN, 256);
		canvas.setFont(font);
		
		canvas.drawLine(400, 300, 1000, 300);
		canvas.drawLine(400, 500, 1000, 500);
		canvas.drawLine(800, 100, 800, 700);
		canvas.drawLine(600, 100, 600, 700);
		
		canvas.drawString("X", 415, 290);
		canvas.drawString("X", 615, 290);
		canvas.drawString("O", 800, 290);
		canvas.drawString("X", 415, 490);
		canvas.drawString("O", 600, 490);
		canvas.drawString("O", 800, 490);
		canvas.drawString("O", 400, 690);
		canvas.drawString("X", 615, 690);
		canvas.drawString("X", 815, 690);
		canvas.drawLine(400, 700, 1000, 100);
	}

}